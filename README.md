Contains utility scripts, studies and results from the service work done for the CLAS12 LTCC simulation, reconstruction and monitoring.

Directory structure:
   * scripts: contains the latest versions of all scripts
   * studies: contains the results of all studies, as well as
              custom versions of the scripts if relevant

It is suggested to frequently document and commit your studies and results.

Keep large data files outside of this repository, but rather save a record on
what you did to create them.