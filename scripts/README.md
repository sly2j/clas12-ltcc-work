#LTCC scripts

## How to run:

~~Ensure the `COATJAVA` environment variable is set, and that you have the
`run-groovy` script in your path.~~

~~Then simply do: `run-groovy <YOUR-SCRIPT.groovy>`~~

For now, we need a custom version of COATJAVA that has a fix for the LTCC 
clustering algorithm implemented. To automatically use this version, simply
execute your script with: `sh run-groovy-path-ifarm.sh <YOUR-SCRIPT.groovy>.
This is only temporary.

## LTCC PID Data Quality (DQ)

Main script: `ltcc-dq.groovy`

Usage: `ltcc-dq.groovy [options] <files.txt|hipo> [files...]`

Arguments:
* `--debug`: Enable debug mode
* `--debug-headless`: Headless mode for debugging (don't display viewer)
* `-e,--events <int>`: Process N events (D: normal: all/debug: 10000.0)
* `-h,--help`: Show usage information
* `--max-mom-electron <double>`: Maximum electron momentum in GeV (D: no cut)
* `--max-mom-pion <double>`: Maximum pion momentum in GeV (D: no cut)
* `--max-nphe <double>`: Maximum number of photo-electrons for both hits and clusters (D: no cut)
* `--max-nphe-cluster <double>`: Maximum nphe cut for clusters only, overrides `--max-nphe`
* `--max-nphe-hit <double>`: Maximum nphe cut for hits only, overrides `--max-nphe`
* `--min-mom-electron <double>`: Minimum electron momentum in GeV (D: 1.0)
* `--min-mom-pion <double>`: Minimum pion momentum in GeV (D: 3.0)
* `--min-nphe <double>`: Minimum number of photo-electrons for both hits and clusters (D: 1.0)
* `--min-nphe-cluster <double>`: Minimum nphe cut for clusters only, overrides `--min-nphe`
* `--min-nphe-hit <double>`: Minimum nphe cut for hits only, overrides `--min-nphe`
* `-t,--tag <identifier>`: Unique identifier for the plots, e.g.  run number (optional)

The program takes an arbitrary number of file arguments. The files are assumed
to be either HIPO files (ending in `.hipo`), or plain text files where each
line is the location of a HIPO file.
