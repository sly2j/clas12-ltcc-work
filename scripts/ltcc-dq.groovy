// UI
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.*
import javax.swing.border.*

// Main Java
import java.util.List
import java.util.ArrayList
import java.util.Arrays
import java.util.LinkedList

// histograms and plotting
import org.jlab.groot.graphics.*
import org.jlab.groot.data.H1F
import org.jlab.groot.data.H2F
import org.jlab.groot.group.DataGroup
import org.jlab.utils.groups.IndexedList

// data sources
import org.jlab.io.base.*
import org.jlab.io.hipo.*

// Cluster finder
import org.jlab.service.ltcc.*

// Particle
import org.jlab.clas.physics.Particle


// groovy imports (CLI builder)
import groovy.util.CliBuilder

// manually apply calibration
import org.jlab.detector.calib.utils.ConstantsManager


// =============================================================================
// GLOBAL SETTINGS AND DEFINITIONS
// =============================================================================
class GLOBAL {
  // GLOBAL logger
  static final String PROGRAM = "LTCC-DQ"
  static def log(String message) {
    println("[$GLOBAL.PROGRAM]: $message")
  }
  static def debug(String message) {
    println("[$GLOBAL.PROGRAM DEBUG]: $message")
  }
  // SECTORS
  static final int N_SECTORS = 4
  static final List<Integer> SECTORS = Arrays.asList(2, 3, 5, 6)
  static final int LEFT_SIDE = 1
  static final int RIGHT_SIDE = 2
  static final int[] SIDES = [LEFT_SIDE, RIGHT_SIDE]
  static String sectorName(int sector, int side = 0) {
    if (side == LEFT_SIDE) {
      return "nPhe Sector $sector (left)"
    } else if (side == RIGHT_SIDE) {
      return "nPhe Sector $sector (right)"
    } else {
      return "nPhe Sector $sector"
    }
  }
  static String sectorTitle(int sector) {
    def name = sectorName(sector)
    return "LTCC $name"
  }
  static final int N_SEGMENTS = 18
  static String segmentName(int segment) {
    return "Segm. $segment"
  } 
  static String segmentTitle(int sector, int side, int segment) {
    def name = segmentName(segment)
    def leftRight = (side == LEFT_SIDE) ? "L" : "R"
    return "LTCC $sector$leftRight $name"
  }
  // particles
  static final List<Integer> PARTICLES = Arrays.asList(211, -211, -11, 11)
  static String particleName(int pid) {
    if (pid == 11) {
      return "e-"
    } else if (pid == -11) {
      return "e+" 
    } else if (pid == -211) {
      return "pi-"
    } else if (pid == 211) {
      return "pi+"
    } else {
      return "unknown particle"
    }
  }
  // other names
  static final int TOTAL_INDEX = 0
  static final int NHIT_INDEX = 1
  static final int TIMING_INDEX = 2
  static final int TIMING_NPHE_INDEX = 3
  static final int EFFICIENCY_INDEX = 4
  static final int TRACK_MOM = 5
  static final int CLUSTER_MOM = 6
  static final int[] SUMMARIES = [TOTAL_INDEX, 
                                  NHIT_INDEX, 
                                  TIMING_INDEX, 
                                  TIMING_NPHE_INDEX, 
                                  EFFICIENCY_INDEX, 
                                  TRACK_MOM, 
                                  CLUSTER_MOM]
  static String summaryName(int idx) {
    if (idx == TOTAL_INDEX) {
      return "nPhe TOTAL"
    } else if (idx == NHIT_INDEX) {
      return "#Hits/Cluster"
    } else if (idx == TIMING_INDEX) {
      return "Delta_t with Center"
    } else if (idx == TIMING_NPHE_INDEX) {
      return "2D nPhe vs Delta_t"
    } else if (idx == EFFICIENCY_INDEX) {
      return "PID Efficiency"
    } else if (idx == TRACK_MOM) {
      return "Track Multiplicity"
    } else if (idx == CLUSTER_MOM) {
      return "Cluster Multiplicity"
    } else {
      return "unknown summary"
    }
  }
  // calibration tables
  static final CCDB = new ConstantsManager()
}

GLOBAL.CCDB.init(["/calibration/ltcc/spe"])

// =============================================================================
// CLI parser and configuration
// =============================================================================
class CliConfig {
  // default values
  static final double MIN_NPHE = 0.0
  static final double MIN_MOM_ELECTRON = 1
  static final double MIN_MOM_PION = 3
  static final double MAX_NPHE = -1
  static final double MAX_MOM_ELECTRON = -1
  static final double MAX_MOM_PION = -1
  static final double EVENTS = 10000

  // configuration variables
  final String tag
  final List<String> inputFiles
  final int events
  // nphe
  final double minNpheHit
  final double minNpheCluster
  final double maxNpheHit
  final double maxNpheCluster
  // momentum
  final double minMomElectron
  final double minMomPion
  final double maxMomElectron
  final double maxMomPion
  // debug
  final Boolean debug
  final Boolean debugHeadless

  CliConfig(String[] args) {
    // define command line arguments
    CliBuilder cli = new CliBuilder(
        usage: "run-groovy ltcc-dq.groovy [options] <files.txt|hipo> [files...]")
    // main setup
    cli.h(longOpt: 'help', args: 0, "Show usage information")
    cli.t(longOpt: 'tag', args: 1, argName: 'identifier',
          "Unique identifier for the plots, e.g. run number (optional)")
    cli.e(longOpt: 'events', args: 1, argName: "int", 
          "Process N events (D: normal: all/debug: $EVENTS)")
    // nphe cuts
    cli._(longOpt: 'min-nphe', args: 1, argName: 'double',
          "Minimum number of photo-electrons for both hits and clusters (D: $MIN_NPHE)")
    cli._(longOpt: 'min-nphe-hit', args: 1, argName: 'double',
          "Minimum nphe cut for hits only, overrides --min-nphe")
    cli._(longOpt: 'min-nphe-cluster', args: 1, argName: 'double',
          "Minimum nphe cut for clusters only, overrides --min-nphe")
    cli._(longOpt: 'max-nphe', args: 1, argName: 'double',
          "Maximum number of photo-electrons for both hits and clusters (D: no cut)")
    cli._(longOpt: 'max-nphe-hit', args: 1, argName: 'double',
          "Maximum nphe cut for hits only, overrides --max-nphe")
    cli._(longOpt: 'max-nphe-cluster', args: 1, argName: 'double',
          "Maximum nphe cut for clusters only, overrides --max-nphe")
    // momentum cuts
    cli._(longOpt: 'min-mom-electron', args: 1, argName: 'double', 
          "Minimum electron momentum in GeV (D: $MIN_MOM_ELECTRON)")
    cli._(longOpt: 'min-mom-pion', args: 1, argName: 'double', 
          "Minimum pion momentum in GeV (D: $MIN_MOM_PION)")
    cli._(longOpt: 'max-mom-electron', args: 1, argName: 'double', 
          "Maximum electron momentum in GeV (D: no cut)")
    cli._(longOpt: 'max-mom-pion', args: 1, argName: 'double', 
          "Maximum pion momentum in GeV (D: no cut)")
    // debug
    cli._(longOpt: 'debug', args: 0, "Enable debug mode")
    cli._(longOpt: 'debug-headless', args: 0,
          "Headless mode for debugging (don't display viewer)")

    // parse command line, exit if issues (or help)
    def options = cli.parse(args)

    // check for errors, and for help
    if (!options) {
      System.exit(1)
    } else if (options.h) {
      cli.usage()
      System.exit(0)
    } else if (options.arguments().isEmpty()) {
      GLOBAL.log("Need to specify at least one HIPO file or file list")
      cli.usage()
      System.exit(1)
    }

    GLOBAL.log("Configuring LTCC DQ script")

    // tag
    this.tag = options.getOptionValue('tag', null)
    if (tag) {
      GLOBAL.log("ID: $tag")
    }

    // nphe cuts
    def minNphe = Double.parseDouble(
      options.getOptionValue('min-nphe', "$MIN_NPHE"))
    this.minNpheHit = Double.parseDouble(
      options.getOptionValue('min-nphe-hit', "$minNphe"))
    this.minNpheCluster = Double.parseDouble(
      options.getOptionValue('min-nphe-cluster', "$minNphe"))
    def maxNphe = Double.parseDouble(
      options.getOptionValue('max-nphe', "$MAX_NPHE"))
    this.maxNpheHit = Double.parseDouble(
      options.getOptionValue('max-nphe-hit', "$maxNphe"))
    this.maxNpheCluster = Double.parseDouble(
      options.getOptionValue('max-nphe-cluster', "$maxNphe"))

    GLOBAL.log("LTCC hit minimum nphe: $minNpheHit")
    if (maxNpheHit > 0) {
      GLOBAL.log("LTCC hit maximum nphe: $maxNpheHit")
    }
    GLOBAL.log("LTCC cluster minimum nphe: $minNpheCluster")
    if (maxNpheCluster > 0) {
      GLOBAL.log("LTCC cluster maximum nphe: $maxNpheCluster")
    }

    // momentum cuts
    this.minMomElectron = Double.parseDouble(
      options.getOptionValue('min-mom-electron', "$MIN_MOM_ELECTRON"))
    this.maxMomElectron = Double.parseDouble(
      options.getOptionValue('max-mom-electron', "$MAX_MOM_ELECTRON"))
    this.minMomPion = Double.parseDouble(
      options.getOptionValue('min-mom-pion', "$MIN_MOM_PION"))
    this.maxMomPion = Double.parseDouble(
      options.getOptionValue('max-mom-pion', "$MAX_MOM_PION"))

    GLOBAL.log("e+/- minimum momentum: $minMomElectron GeV")
    if (this.maxMomElectron > 0) {
      GLOBAL.log("e+/- maximum momentum: $maxMomElectron GeV")
    }
    GLOBAL.log("pi+/- minimum momentum: $minMomPion GeV")
    if (this.maxMomPion > 0) {
      GLOBAL.log("pi+/- maximum momentum: $maxMomPion GeV")
    }

    // debug flags and number of events
    this.debug = (options.debug) ? true : false
    if (debug) {
      this.events = Integer.parseInt(
        options.getOptionValue('events', "$EVENTS"))
      GLOBAL.log("DEBUG mode enabled, restricted to $events events")
    } else {
      this.events = Integer.parseInt(
        options.getOptionValue('events', "-1"))
      if (events > 0) {
        GLOBAL.log("Restricted to $events events")
      }
    }

    this.debugHeadless = (options.'debug-headless') ? true : false
    if (debugHeadless) {
      GLOBAL.log("HEADLESS DEBUG mode enabled")
    }

    // get the files we want to process
    this.inputFiles = new ArrayList()
    for (def fname : options.arguments()) {
      if (fname.endsWith(".hipo")) {
        inputFiles.add(fname)
      } else {
        String[] newFiles = new File(fname)
        for (def f : newFiles) {
          inputFiles.add(f)
        }
      }
    }
    GLOBAL.log("Number of input files: $inputFiles.size")
    // that's all, we're fully configured
  }
}

// =============================================================================
// Sidebar menu
// =============================================================================
class SideBar extends JPanel {
  final JPanel internalPane

  SideBar(List<String> items, ActionListener handler) {
    this.internalPane = new JPanel(new GridLayout(items.size, 1))
    for (def name : items) {
      JButton b = new JButton(name)
      ActionEvent click = new ActionEvent(this, 0, name)
      b.actionPerformed = {handler.actionPerformed(click)}
      this.internalPane.add(b)
    }
    this.add(internalPane, BorderLayout.WEST)
  }
}

// =============================================================================
// Main viewer, see LTCCHistos for the histo definitions
// =============================================================================
class LTCCViewer extends JFrame implements ActionListener {
  final SideBar menu
  final JPanel content
  final JPanel canvasCards

  // DataGroup indexing 
  // * pmtHistos: (sector, side, particle) --> segment
  // * summaryHistos: (histoType, particle) --> sector
  LTCCViewer(LTCCHistos h) {
    // setup window
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    setSize(1200, 800)

    // menu
    menu = new SideBar(menuItems(), this)

    // content window
    content = new JPanel()
    content.setLayout(new BorderLayout())
		content.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED))
    canvasCards = new JPanel(new CardLayout())

    // fill content
    drawPmtHistos(h.pmtHistos)
    drawSummaryHistos(h.summaryHistos)
    content.add(canvasCards)


    // create split pane for display
    JSplitPane splitPane = new JSplitPane(
      JSplitPane.HORIZONTAL_SPLIT, true, menu, content)
    splitPane.setOneTouchExpandable(true)

    // finish setting up the viewer
    this.contentPane.add(splitPane, BorderLayout.CENTER)
    setVisible(true)
  }

  def menuItems() {
    List<String> displayNames = new ArrayList()
    for (def sector : GLOBAL.SECTORS) {
      for (def side : GLOBAL.SIDES) {
        displayNames.add(GLOBAL.sectorName(sector, side))
      }
    }
    for (def sum : GLOBAL.SUMMARIES) {
      displayNames.add(GLOBAL.summaryName(sum))
    }
    return displayNames
  }

  def drawPmtHistos(IndexedList<DataGroup> h) {
    for (def sector : GLOBAL.SECTORS) {
      for (def side : GLOBAL.SIDES) {
        // hacky way to create canvas to suppress the "default" tab
        // that is made when you initiate with empty constructor
        EmbeddedCanvasTabbed c = null
        for (def particle : GLOBAL.PARTICLES) {
          def pName = GLOBAL.particleName(particle)
          if (c == null) {
            c = new EmbeddedCanvasTabbed(pName)
          } else {
            c.addCanvas(pName)
          }
          DataGroup dg = h.getItem(sector, side, particle)
          def tab = c.getCanvas(pName)
          tab.draw(dg)
          style(tab)
          //logY(tab)
        }
        canvasCards.add(c, GLOBAL.sectorName(sector, side))
      }
    }
  }
  def drawSummaryHistos(IndexedList<DataGroup> h) {
    for (def sum : GLOBAL.SUMMARIES) {
      // hacky way to create canvas to suppress the "default" tab
      // that is made when you initiate with empty constructor
      EmbeddedCanvasTabbed c = null
      for (def particle : GLOBAL.PARTICLES) {
        def pName = GLOBAL.particleName(particle)
        if (c == null) {
          c = new EmbeddedCanvasTabbed(pName)
        } else {
          c.addCanvas(pName)
        }
        DataGroup dg = h.getItem(sum, particle)
        def tab = c.getCanvas(pName)
        tab.draw(dg)
        style(tab)
        // fix y-range for efficiency plot, log scale for nphe plots
        if (sum == GLOBAL.EFFICIENCY_INDEX) {
          customRangeY(tab, 0.1, 1.1)
        } else if (sum == GLOBAL.NHIT_INDEX || sum == GLOBAL.TIMING_INDEX) {
          logY(tab)
        } else if (sum == GLOBAL.TIMING_NPHE_INDEX) {
          logZ(tab)
        }
      }
      canvasCards.add(c, GLOBAL.summaryName(sum))
    }
  }

  def style(EmbeddedCanvas c) {
    int nPads = c.NRows * c.NColumns
    for (def i = 0; i < nPads; ++i) {
      c.cd(i)
      c.font = "Avenir"
      c.titleSize = 16
      c.axisTitleSize = 16
      c.axisLabelSize = 14
      c.statBoxFontSize = 14
    }
  }

  def customRangeY(EmbeddedCanvas c, double ymin, double ymax) {
    int nPads = c.NRows * c.NColumns
    for (def i = 0; i < nPads; ++i) {
      c.getPad(i).getAxisY().setRange(ymin, ymax)
    }
  }

  def logY(EmbeddedCanvas c, Boolean enable=true) {
    int nPads = c.NRows * c.NColumns
    for (def i = 0; i < nPads; ++i) {
      c.getPad(i).getAxisY().setLog(enable)
    }
  }

  def logZ(EmbeddedCanvas c, Boolean enable=true) {
    int nPads = c.NRows * c.NColumns
    for (def i = 0; i < nPads; ++i) {
      c.getPad(i).getAxisZ().setLog(enable)
    }
  }

  // handle button press --> show the desired card pane
  void actionPerformed(ActionEvent e) {
    CardLayout c1 = (CardLayout)(canvasCards.layout)
    c1.show(canvasCards, e.actionCommand)
  }
}

// =============================================================================
// Cut Handler
// =============================================================================
class LTCCCutHandler {
  // nphe
  final double minNpheHit
  final double minNpheCluster
  final double maxNpheHit
  final double maxNpheCluster
  // momentum
  final double minMomElectron
  final double minMomPion
  final double maxMomElectron
  final double maxMomPion

  LTCCCutHandler(CliConfig config) {
    // nphe
    this.minNpheHit = config.minNpheHit
    this.minNpheCluster = config.minNpheCluster
    this.maxNpheHit = config.maxNpheHit
    this.maxNpheCluster = config.maxNpheCluster
    // momentum
    this.minMomElectron = config.minMomElectron
    this.minMomPion = config.minMomPion
    this.maxMomElectron = config.maxMomElectron
    this.maxMomPion = config.maxMomPion
  }

  Boolean particleOK(int sector, int pid, double p) {
    if (!GLOBAL.SECTORS.contains(sector)) {
      return false
    } else if (Math.abs(pid) == 11) {
      return (p > minMomElectron && (maxMomElectron < 0 || p < maxMomElectron))
    } else if (Math.abs(pid) == 211) {
      return (p > minMomPion && (maxMomPion < 0 || p < maxMomPion))
    } 
    return false
  }

  Boolean hitOK(LTCCHit hit) {
    return (hit.nphe > minNpheHit && (maxNpheHit < 0 || hit.nphe < maxNpheHit))
  }
  Boolean clusterOK(LTCCCluster cluster) {
    return (cluster.nphe > minNpheCluster 
      && (maxNpheCluster < 0 || cluster.nphe < maxNpheCluster))
  }
}

// =============================================================================
// PID info for all particles
// (see the static member function LTCCParticleInfo.load() that returns
// a list of all particles of interest with PID and LTCC for a given DataEvent
// =============================================================================
class LTCCParticleInfo {
  final int sector
  final int pid
  final double mom
  final double nphe
  final int nhits
  final List<LTCCHit> hits
  static int debug = 0

  LTCCParticleInfo(int sector, int pid, double mom, 
                   LTCCCluster cluster, List<LTCCHit> hits) {
    this.sector = sector
    this.pid = pid
    this.mom = mom
    this.nphe = cluster.nphe
    this.hits = hits
  }
  LTCCParticleInfo(int sector, int pid, double mom) {
    this.sector = sector
    this.pid = pid
    this.mom = mom
    this.nphe = -1
    this.hits = null
  }

  // static function that returns a list of all the particles in this event
  static List<LTCCParticleInfo> load(DataEvent event, LTCCCutHandler cuts) {
    debug += 1
    List<LTCCParticleInfo> info = new ArrayList<>()
    if (event.hasBank("REC::Particle") &&
        event.hasBank("TimeBasedTrkg::TBTracks")) {
      // re-find our clusters because we fixed the reconstruction
      List<LTCCCluster> clusters = new ArrayList<>()
      List<LTCCHit> hits = LTCCHit.loadHits(event, GLOBAL.CCDB)
      if (event.hasBank("LTCC::adc")) {
        // warning: clusterfinder eats incoming hits (maybe should change that)
        clusters = LTCCClusterFinder.findClusters(LTCCHit.loadHits(event, GLOBAL.CCDB))
      }
      // get DC and particle banks
      def bankDC = event.getBank("TimeBasedTrkg::TBTracks")
      def bankPart = event.getBank("REC::Particle")
      final int nPart = bankPart.rows()
      // look at all particles
      for (int ip = 0; ip < nPart; ++ip) {
        def pid = bankPart.getInt("pid", ip)
        def mom = calcMomentum(bankPart, ip)
        // find the sector number for this particle
        final int sector = findSector(bankPart, ip, bankDC)
        // bail if we didn't find a sector
        if (sector < 0) {
          continue
        }
        // fiducial/momentum cut for electrons and pions
        if (!cuts.particleOK(sector, pid, mom)) {
          continue
        }
        // get our selected clusters in this sector
        def selectedClusters =
            getClustersInSector(clusters, sector, cuts)
        // use the highest mult. cluster incase of multiple clusters
        if (selectedClusters.size() > 1) {
          // sort the cluster in decreasing number of phe
          selectedClusters.sort{c1, c2 -> Double.compare(c2.getNphe(), c1.getNphe())}
        }
        // if we have no cluster, add an "empty" entry
        if (selectedClusters.size() < 1) {
          info.add(new LTCCParticleInfo(sector, pid, mom))
        } else {
          def cluster = selectedClusters.get(0)
          def goodHits = matchHits(hits, cluster, cuts)
          info.add(new LTCCParticleInfo(sector, pid, mom, cluster, goodHits))
        }
      }
    }
    return info
  }
  static def findSector(DataBank bankPart, int ip, DataBank bankDC) {
    def sector = -1
    def px = bankPart.getFloat("px", ip)
    def py = bankPart.getFloat("py", ip)
    def pz = bankPart.getFloat("pz", ip)
    def nDC = bankDC.rows()
    for (def iDC = 0; iDC < nDC; ++iDC) {
      // did we find our track?
      if (Math.abs(px - bankDC.getFloat("p0_x", iDC)) < 0.1 &&
          Math.abs(py - bankDC.getFloat("p0_y", iDC)) < 0.1 &&
          Math.abs(pz - bankDC.getFloat("p0_z", iDC)) < 0.1) {
        sector = bankDC.getInt("sector", iDC)
        break
      }
    }
    return sector
  }
  static def calcMomentum(DataBank bankPart, int ip) {
    def px = bankPart.getFloat("px", ip)
    def py = bankPart.getFloat("py", ip)
    def pz = bankPart.getFloat("pz", ip)
    return Math.sqrt(px * px + py * py + pz * pz)
  }

  // get the clusters in this sector
  static def getClustersInSector(List<LTCCCluster> all, int sector, 
                                 LTCCCutHandler cuts) {
    List<LTCCCluster> selected = new ArrayList<>()
    for (def ic = 0; ic < all.size(); ++ic) {
      def cluster = all.get(ic)
      if (cluster.sector == sector && cuts.clusterOK(cluster)) {
        selected.add(cluster)
      }
    }
    return selected
  }

  static def matchHits(List<LTCCHit> hits, LTCCCluster cluster, LTCCCutHandler cuts) {
    List<LTCCHit> matched = new ArrayList()
    // check if the hit is part of the segments inside the cluster
    // (do not need to check phi, as there are only 2 mirrors)
    for (def hit : hits) {
      def theta = hit.position.theta()
      // safety margins, the mirror centers are much further apart anyway
      if (theta >= cluster.thetaMin * 0.99 && theta <= cluster.thetaMax * 1.01
          && cuts.hitOK(hit) && cluster.sector == hit.sector) {
        matched.add(hit)
      }
    }
    return matched
  }
}

// =============================================================================
// Data Analyzer --> creates the histograms
//
// creates 2 IndexedList<DataGroup> as output:
//   * pmtHistos: IndexedList(sector, side, particle) --> DataGroup(segment)
//   * summaryHistos: IndexedList(histoType, particle) --> DataGroup(sector)
//
// used by LTCCViewer
// =============================================================================
class LTCCHistos {
  final IndexedList<DataGroup> pmtHistos 
  final IndexedList<DataGroup> summaryHistos
  final IndexedList<DataGroup> momentumHistos // helper histos for the efficiency

  LTCCHistos() {
    pmtHistos = new IndexedList(3)
    summaryHistos = new IndexedList(2)
    momentumHistos = new IndexedList(2) // (0 -> all, 1 -> LTCC cluster only)
    // PMT histos
    for (def sector : GLOBAL.SECTORS) {
      for (def side : GLOBAL.SIDES) {
        for (def particle : GLOBAL.PARTICLES) {
          def is = 0
          def dg = new DataGroup(5, 4)
          for (def segment = 1; segment <= GLOBAL.N_SEGMENTS; ++segment) {
            def name = GLOBAL.segmentName(segment)
            def title = GLOBAL.segmentTitle(sector, side, segment)
            def pName = GLOBAL.particleName(particle)
            def h = new H1F(name, "nphe($pName)", "Counts", 50, 0, 50)
            h.setTitle("$title")
            h.setFillColor(32 + sector)
            dg.addDataSet(h, is++)
          }
          pmtHistos.add(dg, sector, side, particle) 
        }
      }
    }
    // summary histos, only the total phe for now
    // we calculate the efficiency later from the momentum spectra
    for (sum in [GLOBAL.TOTAL_INDEX, GLOBAL.NHIT_INDEX, GLOBAL.TIMING_INDEX, GLOBAL.TIMING_NPHE_INDEX]) {
      for (def particle : GLOBAL.PARTICLES) {
        def is = 0
        int nRows = 2
        int nCols = (GLOBAL.N_SECTORS + 1) / nRows
        def dg = new DataGroup(nCols, nRows)
        for (def sector : GLOBAL.SECTORS) {
          def name = GLOBAL.sectorName(sector)
          def title = GLOBAL.sectorTitle(sector)
          def pName = GLOBAL.particleName(particle)
          def h = null
          if (sum == GLOBAL.TOTAL_INDEX) {
            h = new H1F(name, "nphe($pName)", "Counts", 50, 0, 50)
          } else if (sum == GLOBAL.NHIT_INDEX) {
            h = new H1F(name, "nhits($pName)", "Counts", 15, 0, 15)
          } else if (sum == GLOBAL.TIMING_INDEX) {
            h = new H1F(name, "Delta_t($pName) [ns]", "Counts", 41, -50, 50)
          } else if (sum == GLOBAL.TIMING_NPHE_INDEX) {
            h = new H2F(name, 41, -50, 50, 50, 0, 50)
            h.getXAxis().setTitle("Delta_t($pName) [ns]")
            h.getYAxis().setTitle("nphe")
          }
          h.setTitle("$title")
          if (h.getClass() == H1F) {
            h.setFillColor(32 + sector)
          }
          dg.addDataSet(h, is++)
        }
        summaryHistos.add(dg, sum, particle)
      }
    }
    // also create helper histos for efficiency
    // (0 -> all rec events, 1 -> LTCC cluster only)
    for (def idx = 0; idx < 2; ++idx) {
      for (def particle : GLOBAL.PARTICLES) {
        def is = 0
        int nRows = 2
        int nCols = (GLOBAL.N_SECTORS + 1) / nRows
        def dg = new DataGroup(nCols, nRows)
        for (def sector : GLOBAL.SECTORS) {
          def name = GLOBAL.sectorName(sector)
          def pName = GLOBAL.particleName(particle)
          def title = ((idx == 0) ? 
            "Track Multiplicity Sector $sector ($pName)" : 
            "LTCC Cluster Multiplicity Sector $sector ($pName)")
          def h = new H1F(name, "P($pName) [GeV]", "Counts", 25, 0, 11)
          h.setTitle("$title")
          h.setFillColor(32 + sector)
          dg.addDataSet(h, is++)
        }
        momentumHistos.add(dg, idx, particle)
      }
    }
  }

  def process(List<LTCCParticleInfo> particles) {
    for (def part : particles) {
      def sectorName = GLOBAL.sectorName(part.sector)
      if (!GLOBAL.PARTICLES.contains(part.pid)) {
        continue
      }
      if (!GLOBAL.SECTORS.contains(part.sector)) {
        continue
      }
      def dgSumTiming = summaryHistos.getItem(GLOBAL.TIMING_INDEX, part.pid)
      def dgSumTimingNphe = summaryHistos.getItem(GLOBAL.TIMING_NPHE_INDEX, part.pid)
      for (def hit : part.hits) {
        def dgHit = pmtHistos.getItem(part.sector, hit.side, part.pid)
        dgHit.getH1F(GLOBAL.segmentName(hit.segment)).fill(hit.nphe)
        def Delta_t = hit.rawTime - part.hits[0].rawTime
        // NOOPEDsuppress first hit
        if (true || Delta_t != 0) {
          dgSumTiming.getH1F(sectorName).fill(Delta_t)
          dgSumTimingNphe.getH2F(sectorName).fill(Delta_t, hit.nphe)
        }
      }
      def dgSumNphe = summaryHistos.getItem(GLOBAL.TOTAL_INDEX, part.pid)
      dgSumNphe.getH1F(sectorName).fill(part.nphe)
      if (part.hits) {
        def dgSumHit = summaryHistos.getItem(GLOBAL.NHIT_INDEX, part.pid)
        dgSumHit.getH1F(sectorName).fill(part.hits.size)
      }
      // also count momentum for efficiencies
      def dgMomAll = momentumHistos.getItem(0, part.pid)
      dgMomAll.getH1F(sectorName).fill(part.mom)
      // nphe cut on cluster already done earlier, just
      // have to check it's not set to -1
      if (part.nphe > 0) {
        def dgMomLTCC = momentumHistos.getItem(1, part.pid)
        dgMomLTCC.getH1F(sectorName).fill(part.mom)
      }
    }
  }
  def calcEfficiencies() {
    def sum = GLOBAL.EFFICIENCY_INDEX
    for (def particle : GLOBAL.PARTICLES) {
      def is = 0
      int nRows = 2
      int nCols = (GLOBAL.N_SECTORS + 1) / nRows
      def dg = new DataGroup(nCols, nRows)
      def dgAll = momentumHistos.getItem(0, particle)
      def dgLTCC = momentumHistos.getItem(1, particle)
      for (def sector : GLOBAL.SECTORS) {
        def name = GLOBAL.sectorName(sector)
        def title = GLOBAL.sectorTitle(sector)
        def pName = GLOBAL.particleName(particle)
        def hAll = dgAll.getH1F(name)
        def hLTCC = dgLTCC.getH1F(name)
        def h = H1F.divide(hLTCC, hAll).getGraph()
        h.setTitle("LTCC Sector $sector PID Efficiency ($pName)")
        h.setTitleX("P($pName) [GeV]")
        h.setTitleY("Efficiency")
        h.setMarkerColor(2 + sector)
        h.setLineColor(2 + sector)
        //h.setLineColor(32 + sector)
        //h.setLineWidth(3)
        dg.addDataSet(h, is++)
      }
      summaryHistos.add(dg, sum, particle)
      summaryHistos.add(dgAll, sum+1, particle)
      summaryHistos.add(dgLTCC, sum+2, particle)
    }
  }
}

def conf = new CliConfig(this.args)
def histos = new LTCCHistos()
def cuts = new LTCCCutHandler(conf)

// TODO add fancy progress bar
int status = 0
GLOBAL.log("Starting event processing")
for (String file : conf.inputFiles) {
  def reader = new HipoDataSource()
  reader.open(file)
  while (reader.hasEvent()) {
    def event = reader.getNextEvent()
    status += 1
    if (!(status % 10000)) {
      GLOBAL.log("Processed $status events")
    }
    if (conf.events > 0 && status > conf.events) {
      break
    }
    def pidInfo = LTCCParticleInfo.load(event, cuts)
    histos.process(pidInfo)
  }
  // are we done with a fixed number of events?
  if (conf.events > 0 && status > conf.events) {
    if (conf.debug) {
      GLOBAL.debug("Reached the requested number of debug events ($conf.events)")
    } else {
      GLOBAL.debug("Reached the requested number of events ($conf.events)")
    }
    break
  }
}

histos.calcEfficiencies()

// headless debug where we don't want to open the viewer
if (conf.debugHeadless) {
  System.exit(0) 
}
def viewer = new LTCCViewer(histos)
