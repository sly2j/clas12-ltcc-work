// data sources
import org.jlab.io.base.*
import org.jlab.io.hipo.*

// groovy imports (CLI builder)
import groovy.util.CliBuilder

// histograms and plotting
import org.jlab.groot.graphics.*
import org.jlab.groot.group.DataGroup
import org.jlab.utils.groups.IndexedList

import org.jlab.groot.data.H1F

// UI
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.*
import javax.swing.border.*

// Main Java
import java.util.List
import java.util.ArrayList
import java.util.Arrays
import java.util.LinkedList

// get file name from CLI
def cli = new CliBuilder(
  usage: "run-groovy track-momentum.groovy <files.txt|hipo> [files...]")
def options = cli.parse(args)

// get the files we want to process
def inputFiles = new ArrayList()
for (def fname : options.arguments()) {
  if (fname.endsWith(".hipo")) {
    inputFiles.add(fname)
  } else {
    String[] newFiles = new File(fname)
    for (def f : newFiles) {
      inputFiles.add(f)
    }
  }
}

final List<Integer> PARTICLES = Arrays.asList(211, -211, -11, 11)
def particleName(int pid) {
  if (pid == 11) {
    return "e-"
  } else if (pid == -11) {
    return "e+" 
  } else if (pid == -211) {
    return "pi-"
  } else if (pid == 211) {
    return "pi+"
  } else {
    return "unknown particle"
  }
}

// output histograms for each particle/sector
List<DataGroup> momHistos = new ArrayList<>()
for (int part : PARTICLES) {
  def dg = new DataGroup(3, 2)
  for (int sector = 1; sector <= 6; ++sector) {
    String name = "S$sector"
    String pName = this.particleName(part)
    def title = "Track Multiplicity for Sector $sector ($pName)"
    def h = new H1F(name, "P [GeV]", "Counts", 25, 0, 11)
    h.setTitle(title)
    h.setFillColor(32 + sector)
    dg.addDataSet(h, sector - 1)
  }
  momHistos.add(dg)
  println(momHistos.size)
}

// utility functions
def calcMomentum(DataBank bankPart, int ip) {
  def px = bankPart.getFloat("px", ip)
  def py = bankPart.getFloat("py", ip)
  def pz = bankPart.getFloat("pz", ip)
  return Math.sqrt(px * px + py * py + pz * pz)
}
static def findSector(DataBank bankPart, int ip, DataBank bankDC) {
  def sector = -1
  def px = bankPart.getFloat("px", ip)
  def py = bankPart.getFloat("py", ip)
  def pz = bankPart.getFloat("pz", ip)
  def nDC = bankDC.rows()

  def pid = bankPart.getInt("pid", ip)
  if (Math.abs(pid) != 211 && Math.abs(pid) != 11) {
    return -1
  }
  for (def iDC = 0; iDC < nDC; ++iDC) {
    // did we find our track?
    def dcx =  bankDC.getFloat("p0_x", iDC)
    def dcy =  bankDC.getFloat("p0_y", iDC)
    def dcz =  bankDC.getFloat("p0_y", iDC)
    if (Math.abs(px - bankDC.getFloat("p0_x", iDC)) < 0.1 &&
        Math.abs(py - bankDC.getFloat("p0_y", iDC)) < 0.1 &&
        Math.abs(pz - bankDC.getFloat("p0_z", iDC)) < 0.1) {
      sector = bankDC.getInt("sector", iDC)
      break
    }
  }
  return sector
}


// loop over all files
int id = 0
int status = 0
for (def file : inputFiles) {
  def reader = new HipoDataSource() 
  reader.open(file)
  while (reader.hasEvent()) {
    status += 1
    if (!(status % 10000)) {
      println("Processed $status events")
    }
    def event = reader.getNextEvent()
    if (event.hasBank("REC::Particle") &&
        event.hasBank("TimeBasedTrkg::TBTracks")) {
      // get DC and particle banks
      def bankDC = event.getBank("TimeBasedTrkg::TBTracks")
      def bankPart = event.getBank("REC::Particle")
      final int nPart = bankPart.rows()
      // look at all particles
      for (int ip = 0; ip < nPart; ++ip) {
        def pid = bankPart.getInt("pid", ip)
        def mom = this.calcMomentum(bankPart, ip)
        int sector = this.findSector(bankPart, ip, bankDC)
        if (sector < 1 || sector > 6) {
          continue
        }
        if (PARTICLES.contains(pid)) {
          int pidx = -1
          if (pid == 211) {
            pidx = 0
          } else if (pid == -211) {
            pidx = 1 
          } else if (pid == -11) {
            pidx = 2
          } else if (pid == 11) {
            pidx = 3
          } else {
            continue
          }
          String name = "S$sector"
          momHistos[pidx].getH1F(name).fill(mom)
        }
      }
    }
  }
}

// display histos
def f = new JFrame()
f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
f.setSize(1200, 800)

EmbeddedCanvasTabbed c = null
int ip = 0
for (def particle : PARTICLES) {
  def pName = particleName(particle)
  if (c == null) {
    c = new EmbeddedCanvasTabbed(pName)
  } else {
    c.addCanvas(pName)
  }
  def tab = c.getCanvas(pName)
  tab.draw(momHistos[ip])
  ip += 1
}
f.add(c)
f.setVisible(true)
