// Data sources
import org.jlab.io.base.DataEvent;
import org.jlab.io.base.DataBank;
import org.jlab.io.base.DataSource;
import org.jlab.io.hipo.HipoDataSource;

// Cluster finder
import org.jlab.service.ltcc.LTCCClusterFinder;
import org.jlab.service.ltcc.LTCCHit;
import org.jlab.service.ltcc.viewer.LTCCHistogrammer;
import org.jlab.service.ltcc.LTCCCluster;

// display
import java.awt.Toolkit;
import javax.swing.JFrame;
import java.awt.Dimension;
import org.jlab.groot.graphics.EmbeddedCanvas;
import org.jlab.groot.graphics.EmbeddedCanvasTabbed;
import org.jlab.groot.base.GStyle;
import org.jlab.groot.group.DataGroup;
import org.jlab.groot.data.H1F;

// Particle
import org.jlab.clas.physics.Particle;

// Other imports
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

class glb {
static public final String RUN = "2977";
static public final String RUN_INFO = "run " + RUN + " (ltcc)";
static public final String[] inputFiles = new File("runlist-"+ RUN + ".txt");

// debug flags
static public final Boolean DEBUG = false;
static public final int N_DEBUG = 10000;
static public final Boolean VERBOSE = false;
};
    

public class PIDInfo {
  private final int sector;
  private final int pid;
  private final double nphe;

  PIDInfo(int sector, int pid, LTCCCluster cluster) {
    this.sector = sector;
    this.pid = pid;
    this.nphe = cluster.getNphe();
  }
  PIDInfo(int sector, int pid) {
    this.sector = sector;
    this.pid = pid;
    this.nphe = -1;
  }
  public double getNphe() { return this.nphe; }
  public int getPID() { return this.pid; }
  public int getSector() { return this.sector; }
  public static List<PIDInfo> load(DataEvent event) {
    List<PIDInfo> info = new ArrayList<>();
    if (event.hasBank("REC::Particle") &&
        event.hasBank("TimeBasedTrkg::TBTracks")) {
      // re-find our clusters because we fixed the reconstruction
      List<LTCCCluster> clusters = new ArrayList<>();
      if (event.hasBank("LTCC::adc")) {
        List<LTCCHit> hits = LTCCHit.loadHits(event);
        clusters = LTCCClusterFinder.findClusters(hits);
      }
      // get DC and particle banks
      DataBank bankDC = event.getBank("TimeBasedTrkg::TBTracks");
      DataBank bankPart = event.getBank("REC::Particle");
      final int nPart = bankPart.rows();
      // look at all particles
      for (int ip = 0; ip < nPart; ++ip) {
        final int pid = bankPart.getInt("pid", ip);
        // momentum cut for electrons and pions
        if ((Math.abs(pid) == 11 && PIDInfo.calcMomentum(bankPart, ip) < 1.0) ||
            (Math.abs(pid) == 211 &&PIDInfo.calcMomentum(bankPart, ip) < 3.0)) {
          continue;
        } else if (glb.DEBUG && glb.VERBOSE) {
          System.out.printf("%d (%d): %f\n", ip, pid,
                            PIDInfo.calcMomentum(bankPart, ip));
        }
        // find the sector number for this particle
        final int sector = PIDInfo.findSector(bankPart, ip, bankDC);
        // bail if we didn't find a sector
        if (sector < 0) {
          continue;
        }
        // get our selected clusters in this sector
        List<LTCCCluster> selectedClusters =
            PIDInfo.getClustersInSector(clusters, sector);
        // use the highest mult. cluster incase of multiple clusters
        if (selectedClusters.size() > 1) {
          // sort the cluster in decreasing number of phe
          selectedClusters.sort{c1, c2 -> Double.compare(c2.getNphe(), c1.getNphe())};
        }
        // if we have no cluster, add an "emtpy" hit
        if (selectedClusters.size() < 1) {
          info.add(new PIDInfo(sector, pid));
        } else {
          info.add(new PIDInfo(sector, pid, selectedClusters.get(0)));
        }
      }
    }
    return info
  }
  private static int findSector(DataBank bankPart, int ip, DataBank bankDC) {
    int sector = -1;
    final double px = bankPart.getFloat("px", ip);
    final double py = bankPart.getFloat("py", ip);
    final double pz = bankPart.getFloat("pz", ip);
    final int nDC = bankDC.rows();
    for (int iDC = 0; iDC < nDC; ++iDC) {
      // did we find our track?
      if (Math.abs(px - bankDC.getFloat("p0_x", iDC)) < 0.1 &&
          Math.abs(py - bankDC.getFloat("p0_y", iDC)) < 0.1 &&
          Math.abs(pz - bankDC.getFloat("p0_z", iDC)) < 0.1) {
        sector = bankDC.getInt("sector", iDC);
        break;
      }
    }
    return sector;
  }
  private static double calcMomentum(DataBank bankPart, int ip) {
    final double px = bankPart.getFloat("px", ip);
    final double py = bankPart.getFloat("py", ip);
    final double pz = bankPart.getFloat("pz", ip);
    return Math.sqrt(px * px + py * py + pz * pz);
  }

  // get the number of clusters in this sector
  private static List<LTCCCluster> getClustersInSector(List<LTCCCluster> all,
                                                 int sector) {
    List<LTCCCluster> selected = new ArrayList<>();
    for (int ic = 0; ic < all.size(); ++ic) {
      if (all.get(ic).getSector() == sector) {
        selected.add(all.get(ic));
      }
    }
    return selected;
  }
};

public class PIDViewer {
  private final List<H1F> helec;
  private final List<H1F> hpion;
  private final List<H1F> hpid;
  private final String ofRoot;

  PIDViewer(String ofRoot=null) {
    helec = new ArrayList<H1F>();
    hpion = new ArrayList<H1F>();
    hpid = new ArrayList<H1F>();
    for (int is = 0; is < 6; ++is) {
      final int sector = is + 1;
      String eName = String.format("Sector %d, e- (%s)", sector, glb.RUN_INFO);
      String pName = String.format("Sector %d, #pi- (%s)", sector, glb.RUN_INFO);
      String pidName = String.format("Sector %d (%s)", sector, glb.RUN_INFO);
      helec.add(new H1F(String.format("he_%d", sector), "LTCC nphe", "Counts",
                        50, 0, 50));
      hpion.add(new H1F(String.format("hp_%d", sector), "LTCC nphe", "Counts",
                        50, 0, 50));
      hpid.add(new H1F(String.format("hpid_%d", sector), "PID", "Counts", 500,
                       -250, 250));
      helec.get(is).setTitle(eName);
      hpion.get(is).setTitle(pName);
      hpid.get(is).setTitle(pidName);
      helec.get(is).setFillColor(33+is);
      hpion.get(is).setFillColor(33+is);
      hpid.get(is).setFillColor(33+is);
    }
    this.ofRoot = ofRoot;
  }

  public void process(List<PIDInfo> pidInfo) {
    for (PIDInfo part : pidInfo) {
      if (part.getPID() == 11) {
        helec.get(part.getSector() - 1).fill(part.getNphe(), 1);
      } else if (part.getPID() == -211) {
        hpion.get(part.getSector() - 1).fill(part.getNphe(), 1);
      }
      hpid.get(part.getSector() - 1).fill(part.getPID());
    }
  }
  public void show() {
    JFrame frame = new JFrame("LTCC PID Info");
    Dimension screensize  = Toolkit.getDefaultToolkit().getScreenSize();
    frame.setMinimumSize(new Dimension(1600, 800));
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    EmbeddedCanvasTabbed canvas = new EmbeddedCanvasTabbed("PID", "Electrons", "Pions");
        
    draw(canvas.getCanvas("PID"), hpid, true);
    draw(canvas.getCanvas("Electrons"), helec);
    draw(canvas.getCanvas("Pions"), hpion);
   
    frame.add(canvas);
    frame.setLocationRelativeTo(null);
    // save if we specified to do so
    if (this.ofRoot != null) {
      for (String cname : Arrays.asList("PID", "Electrons", "Pions")) {
        String ofile = String.format("%s_%s.png", ofRoot, cname);
        canvas.getCanvas(cname).save(ofile);
        System.out.println(ofile);
      }
    }
    frame.setVisible(true);
  }
  private void draw(EmbeddedCanvas canvas, List<H1F> histoList, Boolean logy=false) {
    canvas.divide(3, 2);
    int idx = 0;
    for (H1F h : histoList) {
      canvas.cd(idx);
      setStyle(canvas);
      canvas.getPad(idx).setTitle(h.getTitle());
      canvas.getPad(idx).getAxisY().setLog(logy);
      canvas.draw(h);
      idx += 1;
    }
  }
  private void setStyle(EmbeddedCanvas canvas) {
    canvas.setFont("Avenir");
    canvas.setTitleSize(24);
    canvas.setAxisTitleSize(22);
    canvas.setAxisLabelSize(18);
    canvas.setStatBoxFontSize(18);
  }
}


PIDViewer viewer = new PIDViewer("./plots/LTCC-PID-" + glb.RUN);
int status = 0;
for (String file : glb.inputFiles) {
  DataSource reader = new HipoDataSource();
  reader.open(file);

  // debug: just the first 100 events
  //for (int ie = 0; ie < 1000; ++ie) {
  // else: all events
  while (reader.hasEvent()) {
    DataEvent event = reader.getNextEvent();
    status += 1;
    if (!(status % 10000)) {
      System.out.println(status);
    }
    if (glb.DEBUG) {
      if (glb.VERBOSE) {
        if (event.hasBank("REC::Particle")) {
          System.out.println("REC::Particle found");
        } else {
          System.out.println("REC::Particle not found");
        }
        if (event.hasBank("TimeBasedTrkg::TBTracks")) {
          System.out.println("TimeBasedTrkg::TBTracks found");
        } else {
          System.out.println("TimeBasedTrkg::TBTracks not found");
        }
        if (event.hasBank("LTCC::adc")) {
          System.out.println("LTCC::adc found");
        } else {
          System.out.println("LTCC::adc not found");
        }
      }
      if (status > glb.N_DEBUG) {
        break;
      }
    }
    List<PIDInfo> pidInfo = PIDInfo.load(event);
    viewer.process(pidInfo);
  }
  if (glb.DEBUG) {
    break;
  }
}
viewer.show();

