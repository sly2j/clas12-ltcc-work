import org.jlab.io.base.DataSource;
import org.jlab.io.hipo.HipoDataSource;
import org.jlab.service.ltcc.viewer.LTCCViewer;

String inputfile =
    "/Users/sly2j/Data/CLAS12/pass0/out_clas_002977.evio.10.hipo";

DataSource reader = new HipoDataSource();
reader.open(inputfile);

LTCCViewer viewer = new LTCCViewer();

// dump the first 10 events
for (int i = 0; i < 1; ++i) {
    reader.getNextEvent().getBank("LTCC::adc").show();
}
while (reader.hasEvent()) {
    viewer.process(reader.getNextEvent());
}

viewer.show();
